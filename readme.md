simple-queue-checker
=======================
peter-web (prod) 의 beanstalkd 의 tube 에 남아있는 job 의 갯수를 카운트하기 위한 단순 cli.
네이버카페 전송 정체 상황을 모니터링하기 위해 간단히 구성하였다.

저장소
------
https://iamone6@bitbucket.org/iamonetestteam/simple-queue-checker.git

환경설정
-------
/src/config.php # 단순히 beanstalkd 서버와 포트(default 11300) 명시


tube 리스트 보기
-------------------
>php src/queueCheck.php --list

해당 tube 의 job count 보기
-------------------
>php src/queueCheck.php --tube _{튜브명}_

