<?php
namespace DefaultNameSpace;

include __DIR__."/../vendor/autoload.php";
$conf = include __DIR__."/config.php";
use CmdLine\Parser as Cmdline;
use DefaultNameSpace\Classes\BeanstalkQueue;

$queue = new BeanstalkQueue($conf);
$queue->connect();
//var_dump($queue->status());

# https://packagist.org/packages/jesobreira/cmdline
if (Cmdline::keyexists('list')) {
    // show list of tubes
    $tubes = $queue->list();
    foreach ($tubes as $tube) {
        echo "[{$tube}]\n";
    }
} elseif (Cmdline::keyexists('tube')) {
    // set tube and show job counts
    $queue->setTube(Cmdline::get('tube'));
    echo "count of tube `".Cmdline::get('tube')."` : ".$queue->getCount()." \n";
}