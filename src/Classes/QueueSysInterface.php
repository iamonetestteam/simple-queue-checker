<?php

namespace DefaultNameSpace\Classes;

interface QueueSysInterface
{
    public function connect();
    public function setTube();
    public function list();
    public function getCount();
}