<?php

namespace DefaultNameSpace\Classes;

use Pheanstalk\Pheanstalk;

/**
 * https://developpaper.com/installation-and-usage-of-php-beanstalk-message-queue/
 */
class BeanstalkQueue implements QueueSysInterface
{
    private $queue;
    private $connect;
    private $tube;
    private $server;
    private $port;

    public function __construct(array $conf)
    {
        $this->server = $conf['server'];
        $this->port = $conf['port'];
        $this->queue = new Pheanstalk($this->server, $this->port);
    }

    public function connect()
    {
//        $this->connect = new Pheanstalk($this->server.":".$this->port);
        $this->connect = $this->queue->getConnection();
    }

    public function list() :array
    {
        return $this->queue->listTubes();
    }

    public function setTube(string $tube = 'default')
    {
        // TODO: Implement setTube() method.
        $this->tube = $tube;
        $this->queue = $this->queue->useTube($tube);
    }

    public function getCount()
    {
        // TODO: Implement getCount() method.
        return $this->queue->statsTube($this->tube)['current-jobs-ready'];
    }

    public function status()
    {
        return $this->queue->stats();
    }

}